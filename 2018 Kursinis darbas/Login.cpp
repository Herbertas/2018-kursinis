#include "stdafx.h"
#include "Login.h"


Login::Login()
{
}


Login::~Login()
{
}


void Login::Loading_Users() {
	string temp;

	// Students Buffer
	ifstream fdStudents(All_System_Students);
	while (!fdStudents.eof()) {
		fdStudents >> temp;
		Students.push_back(temp);
	}
	fdStudents.close();

	// Professors Buffer
	ifstream fdProfessors(All_System_Professors);
	while (!fdProfessors.eof()) {
		fdProfessors >> temp;
		Professors.push_back(temp);
	}
	fdProfessors.close();

	// Admins Buffer
	ifstream fdAdmins(All_System_Admin);
	while (!fdAdmins.eof()) {
		fdAdmins >> temp;
		Admins.push_back(temp);
	}
	fdAdmins.close();
}


int Login::Level_Check(string username) {
	switch (username[0]) {
	case '0': {return 0; break; }
	case '1': {return 1; break; }
	case '2': {return 2; break; }
	default: {return 3; break; }
	}
}


pair <bool, USER> Login :: Username_Check(vector<string>& Admin, vector<string>& Lecturers, vector<string>& Students, string username) { // sita reiktu dar patvarkyt
	switch (Level_Check(username)) { // checks if user is admin, lecturer, student
	case 0: {
		for (int i = 0; i < Admin.size(); i++)
			if (Admin[i] == username)
				return make_pair(true, USER::ADMIN);
		return make_pair(false, USER::ADMIN);
		break;
	}
	case 1: {
		for (int i = 0; i < Lecturers.size(); i++)
			if (Lecturers[i] == username)
				return make_pair(true, USER::PROFESSOR);
		return make_pair(false, USER::PROFESSOR);
		break;
	}
	case 2: {
		for (int i = 0; i < Students.size(); i++)
			if (Students[i] == username)
				return make_pair(true, USER::STUDENT);
		return make_pair(false, USER::STUDENT);
		break;
	}
	case 3: {
		return make_pair(false, USER_ERROR);
		break;
	}
	}
}


bool Login :: Password_Check(string username, string password) {
	ifstream fdPsw(Passwords_path + username + ".txt");
	string psw;
	fdPsw >> psw;
	if (password == psw) return true;
	else return false;
}


bool Login :: Is_Digit(string username) {
	for (int i = 0; i < username.length(); i++)
		if (!isdigit(username[i])) return false;
	return true;
}


pair <bool, USER> Login::Start(string username , string password) {

	bool Logged_in = false;
	Loading_Users();
	pair<bool, USER> pair = make_pair(false, USER::USER_ERROR);
	if (Is_Digit(username)){
		pair = Username_Check(Admins, Professors, Students, username);
		if (pair.first) {
			if (Password_Check(username, password)) {
				pair.first = true;
				return pair;
			}

		}
	}

	return pair ;


}