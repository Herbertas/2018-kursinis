#ifndef PERSON_H
#define PERSON_H


#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;


struct PersonalData {
	string Username;
	string Password;
	string Name;
	string Surname;
};




class Person
{
private:
	PersonalData Personal_data;


public:
	Person();
	Person(PersonalData Personal_Data);
	virtual ~Person();
	void SetPersonalData(string Username, string Password, string Name, string Surname); // Klase priskiria zmogui duomenis
	string Get_Username();
	string Get_Name();
	string Get_Surname();
	void Set_Username(string username);
	void Set_Password(string password);
	void Set_Name(string name);
	void Set_Surname(string surname);





};

#endif // !PERSON_H
