#ifndef MAIN_H
#define MAIN_H



#define MAX_STUDENTS_NUMBER				50000
#define MAX_PROFESSORS_NUMBER			1000
#define MAX_SYSTEM_ADMINS_NUMBER		100
#define MAX_FACULTY_NUMBER				50
#define MAX_GROUPS_NUMBER				50
#define MAX_STUDY_PROGRAMS_NUMBER		50
#define MAX_COURSE_NUMBER				6
#define MAX_SUBJECT_GRADES				20
#define MAX_SUBJECT_NUMBER				10



const string Passwords_path;
const string Students_path; 
const string Professors_path; 
const string Admins_path;

const char All_System_Students[] = "Students.txt"; 
const char All_System_Professors[] = "Professors.txt"; 
const char All_System_Admin[] = "Admins.txt"; 

const char All_System_Faculties[] = "Faculties.txt";
const char All_System_Groups[] = "Groups.txt";
const char All_System_Study_Programs[] = "Study_Programs.txt";

const char All_Passwords[] = "Passwords.txt";




enum USER {
	STUDENT = 1,
	PROFESSOR,
	ADMIN, 
	USER_ERROR
};



 /*
 Here is definded Home Page choices
 */

 enum Home_Page_Student_choice {
	 STUDENT_PERSONAL_INFO = 1,
	 STUDENT_SUBJECTS,
	 STUDENT_LOGOUT
 };



 enum Home_Page_Professor_choice {
	PROFESSOR_PERSONAL_INFO = 1,
	PROFESSOR_SET_MARK,
	PROFESSOR_LOGOUT
 };



 enum Home_Page_Admin_choice
 {
	 PERSONAL_INFO = 1,
	 CONFIGURE_STUDENT,
	 CONFIGURE_PROFESSOR,
	 CONFIGURE_FACULTIES,
	 LOGOUT
 };


#endif // !MAIN_H
