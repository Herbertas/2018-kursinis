#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include <vector>
#include "main.h"
#include "Student.h"
#include "Admin.h"
#include "Professor.h"
#include "Login.h"
using namespace std;



class Menu 
{
private: 
	USER user;


public:
	Menu();
	virtual ~Menu();
	void Start();

	pair <string, USER> Login_Page(string &username);

	void Start_Student(Student student);
	void Start_Professor(Professor professor);
	void Start_Admin(Admin admin);

	Home_Page_Student_choice Show_Home_Page_Student();
	Home_Page_Professor_choice Show_Home_Page_Professor();
	Home_Page_Admin_choice Show_Home_Page_Admin();

	void Show_Student_Personal_Info(Student student);
	void Show_Professor_Personal_Info(Professor professor);
	void Show_Admin_Personal_Info(Admin admin);

	void Configure_Student_Page();
	void Configure_Professor_Page();

	void Show_Students_Subjects(Student student);


};

#endif !MENU_H
