#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
#include <fstream>
#include "main.h"
#include <utility>

using namespace std;




class Login
{

private:
	vector <string> Students, Professors, Admins;
	string FileName;
	string Username;
	string Password;


public:
	Login();
	~Login();
	void Loading_Users();
	int Level_Check(string username);
	pair <bool, USER> Username_Check(vector <string> &Admin, vector <string> & Professors, vector<string>& Students, string username);
	bool Password_Check(string username, string password);
	bool Is_Digit(string username);

	pair<bool, USER> Start(string username, string password);

};

