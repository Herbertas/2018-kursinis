#ifndef STUDENT_H
#define STUDENT_H


#include "stdafx.h"
#include "Person.h"
#include <iostream>
#include "main.h"

#define MAX_DALYKU		100

using namespace std;

struct Subject_Struct {
	string Name; 
	int grades[MAX_SUBJECT_GRADES];
	int grades_n = 0;
};
struct Student_structure {

	string Faculty_Name;
	string Study_Program_Name;
	int Course;
	string Group_Name;
	Subject_Struct Subjects[MAX_SUBJECT_NUMBER];
	int subject_n = 0;


};



class Student : public Person
{

private:
	Student_structure Student_data;



public:

	Student();
	Student(string faaculty_name, string study_program_name, int course, string group_name);
	Student(string username);
	string Get_Faculty();
	string Get_Sudy_Program();
	int Get_Course();
	string Get_Group();
	void Set_Faculty(string faculty_name);
	void Set_Course(int course);
	void Set_Group(string group_name);
	
	int Get_Subjects_Number();
	int Get_Subject_Grade(int n);

	virtual ~Student();
};

#endif // !STUDENT_H
