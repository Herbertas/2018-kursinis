#include "stdafx.h"
#include "Menu.h"

using namespace std;

Menu::Menu()
{
}


Menu::~Menu()
{
}

void Menu::Start() {

	pair<string, USER> pair;
	string username;

	pair = Login_Page(username); // Login is here

	bool Logged = true;


	while (Logged) {

	


		if (pair.second == STUDENT) {

			// Sukrauti studentui duomenis 
			Student Student(pair.first);
			Start_Student(Student);
			Logged = false;

		}

		else if (pair.second == PROFESSOR) {
			// Sukrauti duomenis i proffesor

			Professor Professor(pair.first);
			Start_Professor(Professor);
			Logged = false;
		}




		else if (pair.second == ADMIN) {
			Admin Admin(pair.first);
			Start_Admin(Admin);
			Logged = false;
		}



		
	}
}


void Menu::Start_Student(Student student) {
	bool ON = true;

	while (ON) {

		switch (Show_Home_Page_Student()) {

		case Home_Page_Student_choice::STUDENT_PERSONAL_INFO:
			Show_Student_Personal_Info(student);
			break;


		case Home_Page_Student_choice::STUDENT_SUBJECTS:
			// Subjects page
			break;

		case Home_Page_Student_choice::STUDENT_LOGOUT:
			ON = false;
			break;


		default:
				break;


		}

		
	}
}
void Menu::Start_Professor(Professor professor) {
	bool ON = true;

	while (ON) {

		switch (Show_Home_Page_Professor()) {

		case Home_Page_Professor_choice::PROFESSOR_PERSONAL_INFO:
			Show_Professor_Personal_Info(professor);
			// personal info page
			break;

		case Home_Page_Professor_choice::PROFESSOR_SET_MARK:
			// Subject page
			break;

		case Home_Page_Professor_choice::PROFESSOR_LOGOUT:
			ON = false;
			break;

		default: 
			break;

		}


	}
}



void Menu::Start_Admin(Admin admin) {
	bool ON = true;

	while (ON) {

		switch (Show_Home_Page_Admin()) {

		case  Home_Page_Admin_choice::PERSONAL_INFO:
			//	Personal Page
			break;

		case Home_Page_Admin_choice::CONFIGURE_STUDENT:
			//	Configure Student Page
			break;

		case Home_Page_Admin_choice::CONFIGURE_PROFESSOR:
			//  Configure Professor Page
			break;


		case Home_Page_Admin_choice::LOGOUT:
			// Logout Function
			break;

		default: 
			break;

		}

	}

}



/*
Here is implemented show  home page functions

*/
Home_Page_Student_choice Menu::Show_Home_Page_Student() {

	int choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "						HOME PAGE						   " << endl;
	cout << "Menu:" << endl << endl << endl;
	cout << "1. Your's personal information " << endl;
	cout << "2. Subjects " << endl;
	cout << "3. Logout " << endl << endl << endl;
	cout << "Press optinion number(1-3): ";
	cin >> choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	system("cls");
	return (Home_Page_Student_choice)choice;
}

Home_Page_Professor_choice Menu::Show_Home_Page_Professor() {
	int choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "						HOME PAGE						   " << endl;
	cout << "Menu:" << endl << endl << endl;
	cout << "1. Your's personal information " << endl;
	cout << "2. Subjects " << endl;
	cout << "3. Logout " << endl << endl << endl;
	cout << "Press optinion number(1-3): ";
	cin >> choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	system("cls");
	return (Home_Page_Professor_choice)choice;
}


Home_Page_Admin_choice Menu::Show_Home_Page_Admin() {
	int choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "						HOME PAGE						   " << endl;
	cout << "Menu:" << endl << endl << endl;
	cout << "1. Your's personal information " << endl;
	cout << "2. Configure student " << endl;
	cout << "3. Configure professor " << endl;
	cout << "4. Configure faculties " << endl;
	cout << "5. Logout" << endl << endl << endl;
	cout << "Press optinion number(1-5): ";
	cin >> choice;
	cout << "----------------------------------------------------------" << endl << endl << endl;
	system("cls");
	return (Home_Page_Admin_choice)choice;
}


void Menu::Show_Student_Personal_Info(Student student) {
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "			PERSONAL INFORMATION						   " << endl;
	cout << "Name: " << student.Get_Name() << endl;
	cout << "Surname: " << student.Get_Surname() << endl;
	cout << "Username: " << student.Get_Username() << endl;
	cout << "Study program:" << student.Get_Sudy_Program() << endl;
	cout << "Course:" << student.Get_Course() << endl << endl << endl;
	cout << "Press any key to go back!" << endl;
	system("pause");
	system("cls");
}


void Menu::Show_Professor_Personal_Info(Professor professor) {
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "			PERSONAL INFORMATION						   " << endl;
	cout << "Name: " << professor.Get_Name() << endl;
	cout << "Surname: " << professor.Get_Surname() << endl;
	cout << "Username: " << professor.Get_Username() << endl;
	cout << "Press any key to go back!" << endl;
	system("pause");
	system("cls");
}

void Menu::Show_Admin_Personal_Info(Admin admin) {
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "			PERSONAL INFORMATION						   " << endl;
	cout << "Name: " << admin.Get_Name() << endl;
	cout << "Surname: " << admin.Get_Surname() << endl;
	cout << "Username: " << admin.Get_Username() << endl;
	cout << "Press any key to go back!" << endl;
	system("pause");
	system("cls");
}




pair<string, USER> Menu::Login_Page(string &username) {
	string  password;
	Login Login;
	pair <string, USER> rez;
	pair <bool, USER> pair = make_pair(false, USER::USER_ERROR);
	bool logged_in = false;
	while (!logged_in) {
		cout << "Username:";
		cin >> username;
		cout << endl << "Password:";
		cin >> password;
		pair = Login.Start(username, password);
		if (pair.first) {
			cout << "Correct!" << endl;
			rez.first = username;
			rez.second = pair.second;
			logged_in = true;
		}
		else cout << "Incorrect! Please Try Again!" << endl;
		system("cls");
	}

	return rez;
	
}

void Menu::Configure_Student_Page() {
	cout << "----------------------------------------------------------" << endl << endl << endl;
	cout << "			CONFIGURE STUDENT PAGE						   " << endl;
	
}




void Menu::Show_Students_Subjects(Student student) {
	cout << "Subjects:" << endl;	
	for (int i = 0; i < student.Get_Subjects_Number(); i++) {
		for(int j = 0; j< student.)
	}
}