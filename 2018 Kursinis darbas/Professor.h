#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "Person.h"
#include "Student.h"

struct Professor_structure {
	string Faculty_Name; 
	string Subject;


};




class Professor : public Person
{
private:
	Professor_structure Professor_Data;


public:
	Professor();
	Professor(string username);
	Professor(string name, string surname, string username, string pasword);
	virtual ~Professor();
	void Set_Faculty(string faculty_name);
	string Get_Faculty();
	void Set_Mark(int mark, Student Student);



};
#endif // ! PROFESSOR_H 