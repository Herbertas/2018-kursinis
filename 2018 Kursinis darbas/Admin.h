#ifndef ADMIN_H
#define ADMIN_H

#include "stdafx.h"
#include <iostream>
#include "Person.h"
#include "Student.h"
#include "Professor.h"
#include "main.h"
#include <direct.h>
#include <string>
#include "Professor.h"
#include "Student.h"





class Admin : public  Person
{

public:

	Admin();
	Admin(string name, string surname, string username, string password);
	Admin(string username);
	void Add_Admin();
	void Add_Student();
	void Add_Professor();


	virtual ~Admin();
};
#endif // !ADMIN_H

